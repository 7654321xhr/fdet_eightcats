import os
import json
from PIL import Image
import random

labels = ['head', 'hat', 'person', 'cell_phone', 'cigarette', 'flame']
json_files = [os.path.join('jsons', fname) for fname in os.listdir("jsons/") if fname.endswith('.json')]
random.shuffle(json_files)
for json_file in json_files:
    img_file = json_file.replace('jsons', 'images').replace('.json', '.jpg')

    img = Image.open(img_file).convert('RGB')
    content = json.loads(open(json_file).read())
    shapes = content['shapes']
    print("# %s" % img_file.replace('images/', ''))
    for shape in shapes:
        points = shape['points']
        x1, y1 = points[0]
        x2, y2 = points[1]
        x1 = int(x1)
        y1 = int(y1)
        x2 = int(x2)
        y2 = int(y2)
        w = x2 - x1
        h = y2 - y1

        if shape['label'] not in labels:
            import pdb; pdb.set_trace()
        cls = labels.index(shape['label']) + 1
        print("%d %d %d %d %d -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 -1.0 0.2" % (cls, x1,y1,w, h))
