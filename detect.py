from __future__ import print_function
import json
import os
import argparse
import torch
import torch.backends.cudnn as cudnn
import numpy as np
import time
from data import cfg_mnet, cfg_slim, cfg_rfb, cfg_resnet
from layers.functions.prior_box import PriorBox
from utils.nms.py_cpu_nms import py_cpu_nms
import cv2
from models.retinaface import RetinaFace
from models.net_slim import Slim
from models.net_rfb import RFB
from utils.box_utils import decode, decode_landm
from utils.timer import Timer
import random


parser = argparse.ArgumentParser(description='Test')
parser.add_argument('-m', '--trained_model', default='./weights/RBF_Final.pth',
                    type=str, help='Trained state_dict file path to open')
parser.add_argument('--network', default='RFB', help='Backbone network mobile0.25 or slim or RFB')
parser.add_argument('img_dir',type=str)
parser.add_argument('output_file',type=str)
parser.add_argument('output_dir',type=str)
parser.add_argument('--origin_size', default=True, type=str, help='Whether use origin image size to evaluate')
parser.add_argument('--long_side', default=640, help='when origin_size is false, long_side is scaled size(320 or 640 for long side)')
parser.add_argument('--save_folder', default='./widerface_evaluate/widerface_txt/', type=str, help='Dir to save txt results')
parser.add_argument('--cpu', action="store_true", default=False, help='Use cpu inference')
parser.add_argument('--confidence_threshold', default=0.02, type=float, help='confidence_threshold')
parser.add_argument('--top_k', default=5000, type=int, help='top_k')
parser.add_argument('--img_sz', default=320, type=int, help='detect image size')
parser.add_argument('--nms_threshold', default=0.4, type=float, help='nms_threshold')
parser.add_argument('--keep_top_k', default=750, type=int, help='keep_top_k')
parser.add_argument('--save_image', action="store_true", default=True, help='show detection results')
parser.add_argument('--vis_thres', default=0.6, type=float, help='visualization_threshold')
args = parser.parse_args()


def check_keys(model, pretrained_state_dict):
    ckpt_keys = set(pretrained_state_dict.keys())
    model_keys = set(model.state_dict().keys())
    used_pretrained_keys = model_keys & ckpt_keys
    unused_pretrained_keys = ckpt_keys - model_keys
    missing_keys = model_keys - ckpt_keys
    print('Missing keys:{}'.format(len(missing_keys)))
    print('Unused checkpoint keys:{}'.format(len(unused_pretrained_keys)))
    print('Used keys:{}'.format(len(used_pretrained_keys)))
    assert len(used_pretrained_keys) > 0, 'load NONE from pretrained checkpoint'
    return True


def remove_prefix(state_dict, prefix):
    ''' Old style model is stored with all names of parameters sharing common prefix 'module.' '''
    print('remove prefix \'{}\''.format(prefix))
    f = lambda x: x.split(prefix, 1)[-1] if x.startswith(prefix) else x
    return {f(key): value for key, value in state_dict.items()}


def load_model(model, pretrained_path, load_to_cpu):
    print('Loading pretrained model from {}'.format(pretrained_path))
    if load_to_cpu:
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage)
    else:
        device = torch.cuda.current_device()
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage.cuda(device))
    if "state_dict" in pretrained_dict.keys():
        pretrained_dict = remove_prefix(pretrained_dict['state_dict'], 'module.')
    else:
        pretrained_dict = remove_prefix(pretrained_dict, 'module.')
    check_keys(model, pretrained_dict)
    model.load_state_dict(pretrained_dict, strict=False)
    return model


if __name__ == '__main__':
    labels = ['hat', 'head', 'person', 'cell_phone', 'cigarette', 'flame', 'tshirt', 'shorts', 'ower the fence']

    torch.set_grad_enabled(False)

    cfg = None
    net = None
    if args.network == "mobile0.25":
        cfg = cfg_mnet
        net = RetinaFace(cfg = cfg, phase = 'test')
    elif args.network == "Resnet50":
        cfg = cfg_resnet
        net = RetinaFace(cfg = cfg, phase = 'test')
    elif args.network == "slim":
        cfg = cfg_slim
        net = Slim(cfg = cfg, phase = 'test')
    elif args.network == "RFB":
        cfg = cfg_rfb
        net = RFB(cfg = cfg, phase = 'test')
    else:
        print("Don't support network!")
        exit(0)

    net = load_model(net, args.trained_model, args.cpu)
    net.eval()
    print('Finished loading model!')
    print(net)
    cudnn.benchmark = True
    device = torch.device("cpu" if args.cpu else "cuda")
    net = net.to(device)

    # testing begin
    #fnames = [fname for fname in os.listdir('/data/six_cats/val/') if fname.endswith('.jpg')]
    i = 0
    fnames = [fname for fname in os.listdir(args.img_dir) if fname.endswith('.jpg')]
    fnames.sort()
   # print(fnames)
    #random.shuffle(fnames)
    coco_preds=[]
    os.makedirs(args.output_dir, exist_ok=True)
    with open(args.output_file,'w') as f:
    #for i in range(100):
      for fname in fnames:
        image_path = os.path.join(args.img_dir, fname)
        
        img_raw = cv2.imread(image_path, cv2.IMREAD_COLOR)
        raw_wid = img_raw.shape[1]
        raw_hei = img_raw.shape[0]
        new_wid = float(args.img_sz) / max(raw_wid, raw_hei) * raw_wid
        new_hei = float(args.img_sz) / max(raw_wid, raw_hei) * raw_hei
        rsz_img = cv2.resize(img_raw, (int(new_wid), int(new_hei)))
        img = np.float32(rsz_img)

        # testing scale
        target_size = args.long_side
        max_size = args.long_side
        im_shape = img.shape
        im_size_min = np.min(im_shape[0:2])
        im_size_max = np.max(im_shape[0:2])
        resize = float(target_size) / float(im_size_min)
        # prevent bigger axis from being more than max_size:
        if np.round(resize * im_size_max) > max_size:
            resize = float(max_size) / float(im_size_max)
        if args.origin_size:
            resize = 1

        if resize != 1:
            img = cv2.resize(img, None, None, fx=resize, fy=resize, interpolation=cv2.INTER_LINEAR)
        im_height, im_width, _ = img.shape


        scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
        img -= (104, 117, 123)
        img = img.transpose(2, 0, 1)
        img = torch.from_numpy(img).unsqueeze(0)
        img = img.to(device)
        scale = scale.to(device)

        tic = time.time()
        loc, conf, landms = net(img)  # forward pass
#        print(loc)
 #       print(conf)
  #      print(landms)
        print('net forward time: {:.4f}'.format(time.time() - tic))

        priorbox = PriorBox(cfg, image_size=(im_height, im_width))
        priors = priorbox.forward()
        priors = priors.to(device)
        #print(priors)
        prior_data = priors.data
        boxes = decode(loc.data.squeeze(0), prior_data, cfg['variance'])
        boxes = boxes * scale / resize
        boxes = boxes.cpu().numpy()
        #print(boxes)
        scores = 1.0 - conf.squeeze(0).data.cpu().numpy()[:,0]
        #print(scores)
        _, classes = conf.squeeze(0).data.max(dim=1, keepdim=False)
        classes = classes.cpu().numpy()

        landms = decode_landm(landms.data.squeeze(0), prior_data, cfg['variance'])
        scale1 = torch.Tensor([img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                               img.shape[3], img.shape[2]])
        scale1 = scale1.to(device)
        landms = landms * scale1 / resize
        landms = landms.cpu().numpy()

        # ignore low scores
        inds = np.where(scores > args.confidence_threshold)[0]
        boxes = boxes[inds]
        #print(boxes)
        landms = landms[inds]
        scores = scores[inds]
        classes = classes[inds]

        # keep top-K before NMS
        order = scores.argsort()[::-1][:args.top_k]
        boxes = boxes[order]
        landms = landms[order]
        scores = scores[order]
#        print(scores)
        classes = classes[order]

        # do NMS
        dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
        keep = py_cpu_nms(dets, args.nms_threshold)
        # keep = nms(dets, args.nms_threshold,force_cpu=args.cpu)
        dets = dets[keep, :]
        landms = landms[keep]
        classes = classes[keep]

        # keep top-K faster NMS
        dets = dets[:args.keep_top_k, :]
        landms = landms[:args.keep_top_k, :]
        classes = classes[:args.keep_top_k]

        dets = np.concatenate((dets, landms, classes.reshape(-1,1)), axis=1)
        #print(dets)
        # show image
        
        if args.save_image:
            for b in dets:
         #     print(b[4])
              if b[4] > args.vis_thres:
                presocre=(b[4])    #continue
                #text = "{:.4f}".format(b[4])
                cls = int(b[-1])
                #text = "%s %.2f" % (labels[cls-1][:2], b[4])
                text = labels[cls-1]
                #print(text)
                #print(cls-1)
                b = list(map(int, b))
                #print(b)
                sc = max(raw_wid,raw_hei) / float(args.img_sz)
                cv2.rectangle(rsz_img, (b[0], b[1]), (b[2], b[3]), (0, 0, 255), 2)
                cx = b[0] #int((b[0] + b[2])/2.0)
                cy = b[1] #int((b[1] + b[3])/2.0)
                cv2.putText(rsz_img, text, (cx, cy),
                            cv2.FONT_HERSHEY_DUPLEX, 0.5, (0, 255, 0))
                coco_preds_one_frame = {'image_id':i,'category_id':(cls-1),'bbox':[b[0]*sc,b[1]*sc,(b[2]-b[0])*sc,(b[3]-b[1])*sc],'score':presocre,'image_file':fname}
                #print(coco_preds_one_frame)
                #for ins in coco_preds_one_frame:
                    #print(os.path.basename(fname))
                  #  if ins == 'image_file':
                 #      coco_preds_one_frame[ins]=str(os.path.basename(fname))
                coco_preds.append(coco_preds_one_frame)
                #print(i)
                # landms
                #cv2.circle(rsz_img, (b[5], b[6]), 1, (0, 0, 255), 4)
                #cv2.circle(rsz_img, (b[7], b[8]), 1, (0, 255, 255), 4)
                #cv2.circle(rsz_img, (b[9], b[10]), 1, (255, 0, 255), 4)
                #cv2.circle(rsz_img, (b[11], b[12]), 1, (0, 255, 0), 4)
                #cv2.circle(rsz_img, (b[13], b[14]), 1, (255, 0, 0), 4)
            # save image
            
            #json.dump(coco_preds, f)
            #cv2.resize(rsz_img, (0,0),fx=sc,fy=sc)
            name = os.path.join(args.output_dir, fname) # "test.jpg"
            cv2.imwrite(name, rsz_img)
            i = i + 1
      json.dump(coco_preds, f)
