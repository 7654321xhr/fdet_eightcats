#---------  weights_mobile0.25_aug_6anks_v0 ------------
# train
#mkdir -p weights_mobile0.25_aug_6anks_v0
#CUDA_VISIBLE_DEVICES=1 nohup python train.py --network mobile0.25 --training_dataset data/six_cats_aug/train/label.txt --save_folder weights_mobile0.25_aug_6anks_v0/  > weights_mobile0.25_aug_6anks_v0/log.txt &

#CUDA_VISIBLE_DEVICES=3 python detect.py -m weights_mobile0.25_aug_6anks_v0/mobilenet0.25_epoch_70.pth --network mobile0.25

#--------- weights_mobile0.25_aug_6anks_v1 ------------
# train
mkdir -p weights_mobile0.25_aug_6anks_v1
CUDA_VISIBLE_DEVICES=2 nohup python train.py --resume_net weights_mobile0.25_aug_6anks_v0/mobilenet0.25_epoch_110.pth --network mobile0.25 --training_dataset data/six_cats_aug/train/label.txt --save_folder weights_mobile0.25_aug_6anks_v1/  > weights_mobile0.25_aug_6anks_v1/log.txt &

#CUDA_VISIBLE_DEVICES=1 python detect.py -m weights_mobile0.25_aug_6anks_v1/mobilenet0.25_epoch_30.pth --network mobile0.25
